---
title: Crown Minerals (Moratorium on Fracking) Amendment Act 2019
bill: 228
introducer: imnofox
author: 
- imnofox (Greens)
party: Greens
portfolio: Member's Bills
assent: 2019-12-21
commencement: 2019-12-22
first_reading: reddit.com/r/ModelNZParliament/comments/e0eea1/b228_crown_minerals_moratorium_on_fracking/
committee: reddit.com/r/ModelNZParliament/comments/e3a9ng/b228_crown_minerals_moratorium_on_fracking/
final_reading: reddit.com/r/ModelNZParliament/comments/e6uyof/b228_crown_minerals_moratorium_on_fracking/
amends:
-  Crown Minerals Act 1991
---

# Crown Minerals (Moratorium on Fracking) Amendment Act 2019

**1. Title**

This Act is the Crown Minerals (Moratorium on Fracking) Amendment Act 2019.

**2. Purpose**

The purpose of this Act is to restore the indefinite moratorium on fracking.

**3. Principal Act**

This Act amends the Crown Minerals Act 1991 (the **principal Act**).
 
**4. Section 2 amended (Interpretation)**

In section 2(1), insert the following definition in the appropriate alphabetical order:

***

> **fracking** means the injection of fluid into shale beds at high pressure in order to free up petroleum resources

***

**5. Section 23A amended (Application for permits)**

In section 23A, insert as subsection (2):

***

> 2) However, a person may not apply under this section for a mining permit for petroleum if intending to use the method of fracking.

***
