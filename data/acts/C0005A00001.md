---
title: Criminal Records (Expungement of Convictions for Historical Drug and Homosexual Offences) Act 2018
bill: 63
introducer: UncookedMeatloaf
party: Greens
portfolio: Member's Bills
assent: 2018-11-19
commencement: 2018-11-20
urls:
- "https://reddit.com/r/ModelNZParliament/comments/9k4l2g/b63_criminal_records_expungement_of_convictions/"
amends:
- Criminal Records (Clean Slate) Act 2004
---

# Criminal Records (Expungement of Convictions for Historical Cannabis Offences) Act 2018

**1. Title**

This Act is the Criminal Records (Expungement of Convictions for Historical Drug and Homosexual Offences) Act 2018.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Purpose**

The purpose of this Act is to reduce prejudice, stigma, and all other negative effects, arising from a conviction for a historical drug and homosexual offence by-

> a) enabling an application for expungement of the conviction to be made under this Act by an eligible person (before that person’s death) or a representative (after the eligible person's death); and 
> 
> b) expunging the conviction if the Secretary’s decision on the application is that, on the balance of probabilities, the conviction meets the test for expungement.

**4. Interpretation**

In this Act, unless the context otherwise requires,-

**controlling public office** has the meaning given in section 4 of the Public Records Act 2005

**convicted person**, in relation to a conviction, means the person against whom the conviction is entered

**criminal record**, of a conviction for a historical offence, means any public record (including an electronic public record) that is kept by, or on behalf of, the Crown (including by a government department or law enforcement agency) of-

> a) charges that result in the conviction; and 
> 
> b) the conviction as entered (including any item on a list of previous convictions); and 
> 
> c) sentences imposed or other dispositions of the case (including any item on a list of previous sentences); and 
> 
> d) orders that, as a result of the conviction, are imposed on, or made in respect of, the convicted person or any other offender

**eligible person**, for a conviction for a historical offence, means the convicted person

**expunged conviction**- 

> a) means a conviction that-
> 
> > i) has been expunged under section 8; and 
> > 
> > ii) has not ceased to be an expunged conviction under section 19; and 
> 
> b) in section 9, includes any criminal record of a conviction of that kind

**government department** means a department named in Schedule 1 of the State Sector Act 1988

**historical cannabis offence** or **historical offence** has the meaning given to it by section 5

**including** means including without limitation (to the matters specified)

**law enforcement agency** means-

> a) an agency that holds, or has access to, information described in Schedule 5 of the Privacy Act 1993; and 
> 
> b) the Ministry of Business, Innovation, and Employment, the Inland Revenue Department, and the New Zealand Customs Service

**legal proceeding** means-

> a) a proceeding conducted by a court, or by a person acting judicially; and 
> 
> b) any interlocutory or other application to a court, or to a person acting judicially, and connected with that proceeding

**officer** includes a chief executive officer

**post** includes any office or position

**public record** has the meaning given in section 4 of the Public Records Act 2005

**representative**, for a conviction for a historical offence, after the convicted person's death, means any of the following: 

> a) the executor, administrator, or trustee of, acting on behalf of, the estate of the convicted person;
> 
> b) a spouse, civil union partner, or de facto partner, of the convicted person;
> 
> c) a parent, sibling, or child, of the convicted person; 
> 
> d) a person who the Secretary has decided under section 15 can represent the convicted person for an application for expungement of the conviction (and see also section 14(2))

**Secretary** means the Secretary for Justice.

**5. Historical offence defined**

1) In this Act, unless the context otherwise requires, historical offence means (whenever the plea or finding of guilt, or conviction, was made or entered) an offence against, or involving, any of--

> a) the relevant repealed sections of the Misuse of Drugs Act 1975 as repealed by the Misuse of Drugs (Recreational Cannabis) Amendment Act 2017, as in force at any time before 20 December 2017 (which is the date on which the Misuse of Drugs (Recreational Cannabis) Amendment Act 2017 came into force);
> 
> b) the relevant sections of the Misuse of Drugs Act 1975 as repealed by the Misuse of Drugs (Select Hallucinogenic Substances) Amendment Act 2018, as in force at any time before 1 September 2018 (which is the date on which the Misuse of Drugs (Select Hallucinogenic Substances) Amendment Act 2018 came into force);
> 
> c) the relevant repealed sections of the Crimes Act 1961 and the Crimes Act 1908, as repealed by the Homosexual Law Reform Act 1986, as in force at any time-
> 
> > i) on or after 4 August 1908 (which is the date on which the Crimes Act 1908 came into operation); and
> > 
> > ii) before 8 August 1986 (which is the date on which the Homosexual Law Reform Act 1986 came into force).

2) The definition in subsection (1) applies regardless of whether the offence was committed in all or any of the following ways (if applicable):

> a) as any, or the only, principal offender for the offence, or as a party to the offence;
> 
> b) as an attempt to commit the offence (unless the offence is itself specified as, or provides it may be completed on, an attempt);
> 
> c) by way of a conspiracy to commit the offence (alone, or with 1 or more other offences);
> 
> d) by being an accessory after the fact in relation to the offence.

**5A. Test for expungement**

1) A conviction for a historical offence is expunged if-

> a) an application for expungement of the conviction is made under this Act by an eligible person or a representative (see sections 11 and 12); and 
> 
> b) the Secretary's decision on the application is that, on the balance of probabilities, the conviction meets the test for expungement (see section 15).

2) The test is that the conduct constituting the offence, if engaged in when the application was made, would not constitute an offence under the laws of New Zealand.

**6. General effects of expungement**

1) If a conviction for a historical offence is an expunged conviction, its expungement has, for the purposes of the laws of New Zealand, the effects set out in this section.

2) A question about the convicted person's criminal history (including one put in a legal proceeding and required to be answered under oath or affirmation) is to be taken not to refer to any expunged conviction, but to refer only to any conviction that the person has that is not expunged.

3) The convicted person is not required to disclose to any other person for any purpose (including when giving evidence on oath or affirmation in a legal proceeding) information concerning any expunged conviction.

4) In the application to the convicted person of an enactment or arrangement (including an agreement, contract, deed, or trust),-

> a) a reference to a conviction, however expressed, is to be taken not to refer to any expunged conviction; and 
> 
> b) a reference to the convicted person's character or fitness, however expressed, is not to be taken as allowing or requiring account to be taken of any expunged conviction.

5) Any expunged conviction, or the non-disclosure of any expunged conviction, is not a proper ground for-

> a) refusing the convicted person any appointment, post, status, or privilege; or 
> 
> b) revoking any appointment, status, or privilege held by the convicted person, or dismissing the convicted person from any post.

6) The fact that a refusal, revocation, or dismissal of that kind occurred, solely on account of that conviction, before the conviction became an expunged conviction is not a proper ground for a refusal, revocation, or dismissal, solely on account of that conviction, occurring after the expungement.

7) Expungement of a conviction neither authorises, nor requires, destruction of criminal records of the expunged conviction.

**7. Relationship with other laws**

1) This Act does not limit or affect the application to the convicted person of the Criminal Records (Clean Slate) Act 2004.

2) This Act does not limit or affect the application to the convicted person of the Royal prerogative of mercy.

3) This Act does not limit or affect the convicted person's rights under the Privacy Act 1993 or any other enactment to request information about, or a copy of, the convicted person's own criminal record.

**8. Duties of controlling public offices that hold criminal records**

1) This section applies to the chief executive of a controlling public office that holds, or has access to, criminal records.

2) The chief executive must take all reasonable steps to ensure that the office, and any employee or contractor of the office,-

> a) conceals criminal records of an expunged conviction when requests are made for their disclosure other than as authorised under section 10(3); and 
> 
> b) does not use criminal records of an expunged conviction other than for a purpose authorised under this section 10(3).

3) The reasonable steps include the development of policies and procedures.

**9. Effect of expungement on requests to disclose, and use of, criminal records**

1) This section applies to a controlling public office, or any officer, employee, or contractor of a controlling public office, that holds, or has access to, criminal records.

2) The office, officer, employee, or contractor, in responding to a request for the disclosure of a convicted person's criminal record or any information about a convicted person's criminal record, must not disclose the criminal record of an expunged conviction other than as authorised under section 10(3).

3) The office, officer, employee, or contractor is not entitled to use criminal records of an expunged conviction other than for a purpose authorised under section 10(3).

**10. Offence to unlawfully disclose information required to be concealed**

1) This section applies to a person who-

> a) is an officer, employee, or contractor of a controlling public office, government department, or law enforcement agency; and 
> 
> b) has access to criminal records that are held by, or accessible to, the office, department, or agency.

2) The person commits an offence if the person-

> a) discloses to any person, body, or agency the criminal record, or information about the criminal record, of an expunged conviction other than as authorised under subsection (3); and 
> 
> b) discloses that record or that information knowing that the person does not have, or being reckless as to whether or not the person has, lawful authority under subsection (3).

3) Subsection (2) does not apply to or prevent disclosure of the criminal record, or information about the criminal record, and that is all or any of the following: 

> a) any disclosure or communication of (or of information about) criminal records of an expunged conviction, when the convicted person expressly requests the convicted person's criminal history, including any expunged conviction of the convicted person, be disclosed directly to the convicted person under the Privacy Act 1993 or any other enactment referred to in section 7(3): 
> 
> b) any research, or publication, that relates to historical offences, expunged convictions, or both, and that is anonymised (because it does not identify, and is not likely to lead to the identification of, any person who has an expunged conviction): 
> 
> c) any disclosure or communication of (or of information about) criminal records of expunged convictions, when those criminal records are classified as open access records, and made available for inspection by members of the public, publication, or copying, under the Public Records Act 2005: 
> 
> d) any disclosure or communication necessary or desirable for the administration of this Act (including for recording in criminal records that a conviction has become an expunged conviction).

4) A person who commits an offence against subsection (2) is liable on conviction to a fine not exceeding $15,000.

**11. Offence to require or request that individual disregard expungement**

1) A person commits an offence if the person requires or requests that an individual-

> a) disregard the effect of expungement under this Act when answering a question about the individual's criminal history, or disclosing information concerning any convictions of the individual, or both; or 
> 
> b) disregard the effect of expungement under this Act and disclose, or give consent to the disclosure of, any criminal records of an expunged conviction of the individual that are or may be disclosed to the individual under the Privacy Act 1993 or any other enactment referred to in section 7(3).

2) A person who commits an offence against subsection (1) is liable on conviction to a fine not exceeding $7,500.

**12. Application for expungement**

1) An application for expungement of a conviction for a historical offence may be made by-

> a) an eligible person (before that person's death); or 
> 
> b) a representative (after the eligible person's death).

2) A representative, in this section, includes a person who makes, and includes in the application, a request under section 16.

3) The application-

> a) must be made in the form and manner (if any) approved by the Secretary; and 
> 
> b) may include a request under section 13 (see subsection (2)); and 
> 
> c) must include any supporting information, and supporting submissions, the eligible person or representative wishes the Secretary to consider.

4) Nothing in this Act prevents a person (including an agent, a donee of an enduring power of attorney, or a welfare guardian) from acting under this Act on behalf of an eligible person or a representative.

**13. Request to represent deceased convicted person**

1) This section applies to a conviction for a historical offence if the convicted person has died and a person wishes to make an application for expungement of the conviction as a representative under paragraph (d) of the definition of that term in section 4.

2) The person may, by a written request made to the Secretary, ask the Secretary to decide that the person can represent the convicted person for an application for expungement of the conviction.

3) If a person makes a request under this section, the Secretary must decide as soon as is reasonably practicable whether the person can represent the convicted person for an application for expungement of the conviction.

4) The Secretary’s decision must be based on whether the representation concerned would be in the interests of the deceased convicted person.

5) The decision must be in writing copied promptly to the requester.

**14. Further documents, things, or information**

1) This section applies if, in making a decision under section 13, 16, or 17, the Secretary believes on reasonable grounds that any document, thing, or information-

> a) is necessary to enable the Secretary to make the decision; and 
> 
> b) is not available to the Secretary, but is, or may be, in the possession of, under the control of, or available from, a person; and 
> 
> c) is unlikely to be able to be obtained by the Secretary through any means other than a notice under this section.

2) The Secretary may, by written notice given to the person, seek from the person all or any of the following: 

> a) access to, or a copy, duplicate, or reproduction of, or extract from, any document or thing that is or may be relevant to, or to specified aspects of, the decision: 
> 
> b) information or further information (including written evidence given on oath or affirmation and by affidavit) that is or may be relevant to, or to specified aspects of, the decision.

**15. Offence to fail or refuse to comply with notice**

1) A person (other than one specified in subsection (2)) commits an offence if the person, without reasonable excuse,-

> a) fails to produce, or to allow access to, or copying, duplication, or reproduction of, or the taking of extracts from, any document or thing, as required by a notice under section 14, to the extent that the person is capable of doing so: 
> 
> b) refuses to give information, or further information, or to be sworn or to affirm and give evidence, as required by a notice under section 14, to the extent that the person is capable of doing so.

2) Subsection (1) does not apply to the eligible person or representative, or to an officer, employee, or contractor of a controlling public office, government department, or law enforcement agency, that holds, or has access to, criminal records.

3) Every person who commits an offence against subsection (1)(a) or (b) is liable, on conviction, to a fine not exceeding $750.

**16. Secretary decides application**

1) The Secretary must decide an application for expungement by making, in accordance with section 5, a written decision whether the conviction meets the test for expungement.

2) The decision must be copied promptly to the eligible person or representative and, if it is to the effect that the conviction does not meet the test for expungement, must include written reasons for that decision.

**17. Reconsideration of decisions**

1) The Secretary may reconsider a decision made under section 13 or 16.

2) The power to reconsider may be used on all or any of the following grounds: 

> a) further relevant information has become available to the Secretary since the decision was made: 
> 
> b) the decision was made on an application that included, or was supported by, all or any of the following: 
> 
> > i) false or misleading information: 
> > 
> > ii) documents that are false or misleading: 
> 
> c) any other grounds that the Secretary is satisfied make it, or may make it, necessary or desirable to reconsider the decision.

3) The Secretary may appoint an independent reviewer to assist with a reconsideration.

4) The Secretary may, as a result of the reconsideration, confirm, reverse, or modify the decision (and section 16(2) applies, with all necessary modifications, to the reconsideration decision).

5) A conviction that, as a result of a reconsideration decision, is no longer an expunged conviction ceases to be an expunged conviction on and from the date of the reconsideration decision.

**18. Evidence, independence, and process**

In making a decision under section 13, 16, or 27, the Secretary-

> a) may receive as evidence any statement, document, information, or matter that, in the Secretary's opinion, may help the Secretary to make the decision, whether or not it would be admissible in a court of law; and 
> 
> b) must act independently; and 
> 
> c) must decide on the papers, unless the Secretary considers that an oral hearing should be conducted because there are exceptional circumstances and it is appropriate to do so in the interests of justice.

**19. Protection for person providing information**

No person who provides information in, or in support of, an application or request under this Act is criminally or civilly liable for the action of providing the information if that action-

> a) was taken in good faith; and 
> 
> b) was reasonable in the circumstances.

**20. Entitlement to compensation**

1) A person who has an expunged conviction is entitled to each applicable financial compensation as set out in this section.

2) A person who has an expunged conviction who was required to pay a fine or other money on account of committing, or being convicted of, or sentenced for the offence is entitled to receive compensation for the amount of money paid.

3) A person who has an expunged conviction who was sentenced to a prison term on account of committing, or being convicted of, or sentenced for the offence is entitled to receive $55.00 for every week served in prison.

4) A person who has an expunged conviction who served a sentence for, or complied with an order or a direction made against the person because of committing, the offence is entitled to receive up to $2000, at the Secretary's discretion.

5) Nothing in this section prevents a person being entitled to compensation in respect of anything that occurred while the person was serving a sentence or complying with an order or a direction.

**21. Amendments to Criminal Records (Clean Slate) Act 2004**

1) This section amends the Criminal Records (Clean Slate) Act 2004 (the **principal Act**).

2) Amend the principal Act as indicated in Schedule 2.

## Schedule 1

**Transitional, savings, and related provisions**

**1. Effect on legal proceedings**

1) This Act does not prevent-

> a) the completion of a legal proceeding commenced before the commencement of this Act; or 
> 
> b) the commencing of a legal proceeding on or after the commencement of this Act.

2) However, a proceeding in subclause (1)(a) or (b) decided (at first instance, or on any appeal) on or after the commencement of this Act must be decided subject to this Act.

3) This clause applies even if, and to the extent that, the proceeding concerned is amended before, on, or after the commencement of this Act.

## Schedule 2

**Consequential amendments to Criminal Records (Clean Slate) Act 2004**

In section 4, insert in their appropriate alphabetical order: 

***

> **controlling public office** has the meaning given in section 4 of the Public Records Act 2005 
> 
> **public record** has the meaning given in section 4 of the Public Records Act 2005

***

In section 4, definition of criminal record, replace "official record" with "public record".

In section 4, definition of criminal record, replace "electronic record" with "electronic public record".

In the cross-heading above section 15, replace "government departments and law enforcement agencies" with "controlling public offices".

In section 15(1), replace "government department or law enforcement agency" with "controlling public office".

In section 15(2), replace "government department or law enforcement agency-" in each place with "office".

In the heading to section 16, replace "government departments, law enforcement agencies," with "controlling public offices,"

In section 16(1) and (2), replace "government department or law enforcement agency" in each place with "controlling public office".

Before section 17(1), insert: 

***

> 1A) This section applies to a person who-
> 
> > a) is an officer, employee, or contractor of a controlling public office, government department, or law enforcement agency; and 
> > 
> > b) has access to criminal records that are held by, or accessible to, the office, department, or agency.

***

In section 17(1), replace "A person" with "The person".

In section 19(2) and (3)(g), replace "government department or law enforcement agency" in each place with "controlling public office".

In section 20(1) and (2), before "government department or law enforcement agency" in each place, insert "controlling public office or".

In the heading to section 23, replace "2000" with "2011".

In section 23, replace "2000" with "2011".
