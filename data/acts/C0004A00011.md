---
title: Appropration (August - October 2018 Estimates) Act 2018
bill: 76
introducer: toastinrussian
party: National
portfolio: Finance
assent: 2018-09-13
urls:
- "https://reddit.com/r/ModelNZParliament/comments/9b6nxr/b76_appropriation_august_october_2018_estimates/"
---

# Appropration (August - October 2018 Estimates) Act 2018

## Schedule 1 - Appropriations for the August - October 2018 Financial Year

**Revenue**

***

<table>
  <thead>
    <tr>
      <th><b>Category</b></th>
      <th><b>Value (million $)</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Taxation revenue</td>
      <td>82,523</td>
    </tr>
    <tr>
      <td>Other sovereign revenue</td>
      <td>5,462</td>
    </tr>
    <tr>
      <td><b>Total revenue levied through Sovereign power</b></td>
      <td><b>87,985</b></td>
    </tr>
    <tr>
      <td>Sales of goods and services</td>
      <td>19,646</td>
    </tr>
    <tr>
      <td>Interest revenue and dividends</td>
      <td>4,054</td>
    </tr>
    <tr>
      <td>Other revenue</td>
      <td>6,190</td>
    </tr>
    <tr>
      <td><b>Total revenue earned through operations</b></td>
      <td><b>29,890</b></td>
    </tr>
    <tr>
      <td><b>Total revenue (excluding gains)</b></td>
      <td><b>117,875</b></td>
    </tr>
  </tbody>
</table>

***

**Expenditure**

***

<table>
  <thead>
    <tr>
      <th><b>Category</b></th>
      <th><b>Value (million $)</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Social security and welfare</td>
      <td>33,069</td>
    </tr>
    <tr>
      <td>Health</td>
      <td>17,676</td>
    </tr>
    <tr>
      <td>Education</td>
      <td>16,348</td>
    </tr>
    <tr>
      <td>Core government services</td>
      <td>4,722</td>
    </tr>
    <tr>
      <td>Law and order</td>
      <td>4,803</td>
    </tr>
    <tr>
      <td>Transport and communications</td>
      <td>11,439</td>
    </tr>
    <tr>
      <td>Economic and industrial services</td>
      <td>9,175</td>
    </tr>
    <tr>
      <td>Defence</td>
      <td>2,829</td>
    </tr>
    <tr>
      <td>Heritage, culture and recreation</td>
      <td>2,425</td>
    </tr>
    <tr>
      <td>Primary services</td>
      <td>3,081</td>
    </tr>
    <tr>
      <td>Housing and community development</td>
      <td>2,080</td>
    </tr>
    <tr>
      <td>Environmental protection</td>
      <td>516</td>
    </tr>
    <tr>
      <td>GSF pension expenses</td>
      <td>0</td>
    </tr>
    <tr>
      <td>Other</td>
      <td>1,621</td>
    </tr>
    <tr>
      <td>Sovereign debt servicing</td>
      <td>2,770</td>
    </tr>
    <tr>
      <td>Sovereign debt repayment</td>
      <td>4,604</td>
    </tr>
    <tr>
      <td>Operational allowance</td>
      <td>1,000</td>
    </tr>
    <tr>
      <td>Top-down expense adjustment</td>
      <td>-691</td>
    </tr>
    <tr>
      <td><b>Total Crown expenses excluding losses</b></td>
      <td><b>117,471</b></td>
    </tr>
  </tbody>
</table>

***

**Revenue less Expenditure**

***

<table>
  <thead>
    <tr>
      <th><b>Category</b></th>
      <th><b>Value (million $)</b></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Total revenue</td>
      <td>117,875</td>
    </tr>
    <tr>
      <td>Total expenditure</td>
      <td>-117,471</td>
    </tr>
    <tr>
      <td><b>Revenue minus expenditure</b></td>
      <td><b>404</b></td>
    </tr>
  </tbody>
</table>

***
