---
title: Public Housing (Rent to Own) Act 2018
bill: 82
introducer: dyljam
party: Reform
portfolio: Social Development
assent: 2018-09-24
commencement: 2018-09-25
urls:
- "https://www.reddit.com/r/ModelNZParliament/comments/9hcjl1/b82_public_housing_rent_to_own_bill_first_reading/"
amends:
- Housing Corporation Act 1974
---

# Public Housing (Rent to Own) Act 2018

**Purpose**

The purpose of this Act is to facilitate a new program allowing tenants in public housing the opportunity to purchase their dwelling at an affordable price, and to amend necessary legislation.

**1. Title**

This Act is the Public Housing (Rent to Own) Act 2018.

**2. Commencement**

This Act commences on the day after the date on which it receives the Royal Assent.

**3. Principal Act amended**

This Act amends the Housing Corporation Act 1974 (the **principal Act**).

**4. Section 18 amended (Functions of Corporation)**

In section 18(2), insert:

***

> (m) assisting those on low incomes to purchase their own homes.
> 
> (n) facilitating the sale of public housing to those on low incomes.

***

**5. Section 50D amended (Meaning and relevance of social housing reform objectives)**

Replace section 50D(1)(f) with:

***

> (f) the supply of affordable housing is increased throughout New Zealand.

***

**6. Section 50E amended (Minister may enter into transfer contracts as Corporation or subsidiary)**

After section 50E(2), insert:

***

> (3) Tenants currently residing in public housing owned by the Corporation will receive priority in regards to the sale of existing assets.
> 
> (4) Tenants currently residing in the property owned by the Corporation which is deemed suitable for sale will be eligible to purchase the property at a rate below the market value, determined by the Minister.
> 
> > (a) the market value shall be decided by an independent valuer.
> > 
> > (b) should the tenant or the Corporation dispute the valuer’s decision, a second valuer shall be contracted.
> > 
> > (c) the property must be sold based on whichever value is higher, within reason.
> 
> (5) Any asset sold under a social housing transaction to tenants under subsection 4 must be followed by the replacement of that social housing asset within a reasonable timeframe.
> 
> > (a) the replacement of social housing assets may be done through the purchase or construction of a similar asset.
> > 
> > (b) subsection 5 shall not apply should the cost of replacing a social housing asset exceed the price at which it was sold to tenants.

***

**7. Section 50L amended (Publication of social housing transaction)**

1) Repeal section 50L(1)(b).

2) Replace section 50L(2)(b) with:

***

> (b) ensure that the notice remains on the site and accessible to members of the public for a minimum of six (6) weeks.

***

**8. Section 50Q amended (Delegation)**

Replace section 50Q(1) with:

***

> (1) The Minister may delegate all or any of the Minister’s functions and powers under this part to the chief executive of the Ministry, or the chief executive of the Corporation

***
