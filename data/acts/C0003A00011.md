---
title: Sentencing (Livestock Rustling) Amendment Act 2018
bill: 51
introducer: Kingethan15
party: National
portfolio: Primary Industries
assent: 2018-06-27
commencement: 2020-06-28
urls:
- "https://reddit.com/r/ModelNZParliament/comments/8t5cwv/b51_sentencing_livestock_rustling_amendment_bill/"
amends:
- Sentencing Act 2002
---

#Sentencing (Livestock Rustling) Amendment Act 2018

**Purpose**

Livestock rustling (theft of livestock from farms or property) has recently become much more prevalent in New Zealand and is now at a level that is creating serious risk to farmers and their businesses. It is estimated to cost the farming community over $120 million per year. This activity not only is a threat to farming businesses but also creates risk to people’s safety in more isolated parts of rural New Zealand, as often rustlers are armed and equipped with quite sophisticated tools to assist them.

This bill is designed to deter people from engaging in livestock rustling, by identifying it as an aggravating factor at sentencing. This bill will give more confidence to victims of livestock rustling that there is an additional deterrent in place to discourage this type of crime.

**1. Title**

This Act is the Sentencing (Livestock Rustling) Amendment Act 2018.

**2. Commencement**

This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act**

This Act amends the Sentencing Act 2002 (the **principal Act**).

**4. Section 9 amended (Aggravating and mitigating factors)**

1) After section 9(1)﻿(c), insert:

***

> (ca) that the offence involved theft of livestock:

***

2) After section 9(4), insert:

***

> (4AA) In subsection (1)﻿(ca), livestock means animals kept as part of an agricultural operation, whether for commercial purposes or for private use.

***
