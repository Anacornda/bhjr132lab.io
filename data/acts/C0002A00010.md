---
title: Income Tax (Clean Transport Fringe Benefit Tax Exclusions) Amendment Act 2018
introducer: imnofox
party: Greens
portfolio: Business
assent: 2018-03-04
commencement: 2018-03-05
urls:
- "https://reddit.com/r/ModelNZParliament/comments/817n4n/b18_income_tax_clean_transport_fringe_benefit_tax/"
amends:
- Income Tax Act 2007
---

#Income Tax (Clean Transport Fringe Benefit Tax Exclusions) Amendment Act 2018

**1. Purpose**

The purpose of this Act is to incentivise businesses to support clean modes of transport by removing the fringe benefit tax from public transport passes provided to staff and from zero-emissions vehicles provided to staff.

**2. Commencement**

1) This Act comes into force on the day after the date on which it receives the Royal assent.

**3. Principal Act**

1) This Act amends the Income Tax Act (2007) (the **principal Act**).

**4. Section CX 6 amended (Private use of motor vehicle)**

1) In section CX 6, after subsection (2), insert:

***

> *Exclusion: electric vehicles*  
>
> (2A) Subsection (1) does not apply if the motor power of the vehicle is wholly or partly derived from an external source of electricity.

***

**5. Section CX 9 amended (Subsidised transport)**

1) In section CX 9, after subsection (1), insert:

***

> *Exclusion: public transport passes*  
>
> (2) This section does not apply if the transport is subsided with a public transport pass.

***
