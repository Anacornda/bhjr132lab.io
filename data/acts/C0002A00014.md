---
title: Freedom of Expression Protection Act 2018
introducer: Fresh3001
party: ACT
portfolio: Member's Bills
assent: 2018-03-21
urls:
- "https://reddit.com/r/ModelNZParliament/comments/84xvgi/b15_freedom_of_expression_protection_bill_third/"
amends:
- Crimes Act 1961
- Harmful Digital Communications Act 2015
---

#Freedom of Expression Protection Act 2018

**1. Purpose**

The purpose of this Act is to amend the Crimes Act 1961 so that ‘blasphemous libel’ is no longer illegal, and the Harmful Digital Communications Act 2015 so that people cannot be convicted simply for being offensive, inciting people to be offensive, or for disclosing personal information given in confidence, thus furthering the right to freedom of expression under New Zealand law. It will also amend the Harmful Digital Communications Act 2015 so that the District Court may dismiss cases which involve a media website, thus furthering the right to freedom of the press. The Act will also amend the subject acts in some sections to improve the quality of the legislation.

## Part 1 - Amendments to Crimes Act 1961

**2. Principal Act**

This Part amends the Crimes Act 1961 (the **principal Act**).

**3. Section 123 repealed (Blasphemous libel)**

1) Repeal Section 123.

## Part 2 - Amendments to Harmful Digital Communications Act 2015

**4. Principal Act**

This Part amends the Harmful Digital Communications Act 2015 (the **principal Act**).

**5. Section 6 amended (Communication principles)**

1) Replace Section 6 with the following:

***

> *Principle 1*  
> A digital communication should not disclose sensitive personal facts about an individual to facilitate the breach of the following communication principles.
> 
>*Principle 2*    
> A digital communication should not be threatening, intimidating, or menacing.
> 
> *Principle 3*  
> A digital communication should not be indecent or obscene.
> 
> *Principle 4*  
> A digital communication should not be used to harass an individual.
> 
> *Principle 5*  
> A digital communication should not make a false allegation.
> 
> *Principle 6*  
> A digital communication should not incite or encourage an individual to commit suicide or physical self harm.
> 
> *Principle 7*  
> A digital communication should not abuse an individual by reason of his or her colour, race, ethnic or national origins, religion, gender identity, sexual orientation, or disability.
> 
> *Principle 8*  
> A digital communication should not incite or encourage anyone to send a message that would breach 1 or more of the communication principles of this act.
> 
> 2) For the purpose of this section—
> 
> **harass** means to subject another person to repeated aggressive pressure or repeated grossly offensive messages through direct digital communication.

***

**6. Section 12 amended (Threshold for proceedings)**

1) After section 12(4), insert:

***

> (5) The court may, on its own initiative, dismiss an application under section 11 if the digital communication was published publicly on or by a website that a reasonable person would consider to be a news website, media website, or a website that a reasonable person would consider to be part of the media.

***

**7. New section 13A inserted (Court may require Approved Agency to provide information)**

1) After section 13, insert: 

***

> **13A Court may require Approved Agency to provide information**
> 
> 1) A District Court or any Registrar or Deputy Registrar of the court may require the Approved Agency to provide information for the purposes of satisfying the court of any matters referred to in sections 12 and 13.  
> 2) The Approved Agency must provide the information in the form (if any) prescribed by rules of court.

***

**8. Section 22 amended (Causing harm by posting digital communication)**

1) Replace Section 22(1) with the following:

***

> 1) A person commits an offence if—  
> > a) the person posts a digital communication that breaches the communication principles of section 6 with the intent to cause harm to a victim; and  
> > b) posting the communication would cause harm to an ordinary reasonable person in the position of the victim; and  
> > c) posting the communication causes harm to the victim.

***
