---
title: Misuse of Drugs (Recreational Cannabis) Amendment Act 2017
introducer: Fresh3001
party: ACT
portfolio: Health
assent: 2017-12-20
urls:
- "https://reddit.com/r/ModelNZParliament/comments/7k8skp/b7_misuse_of_drugs_recreational_cannabis/"
amends:
- Misuse of Drugs Act 1975
---

#Misuse of Drugs (Recreational Cannabis) Amendment Act 2017

**1. Purpose**

The purpose of this Act is to amend the Misuse of Drugs Act 1975, the Misuse of Drugs Amendment Act 1978 and the Misuse of Drugs (Prohibition of Cannabis Utensils and Methamphetamine Utensils) Notice 2003 so that cannabis plant, its derivatives and accessories are no longer illegal, and to lay out the legal framework for the subsequent use, transport, cultivation and sale of cannabis plant, its derivatives and accessories.

**2. Amendment to Section 2 of the Misuse of Drugs Act 1975**

1) In Section 2 (1), where ‘prohibited plant’ is defined, repeal the following:

***

> "(a) any plant of the genus Cannabis:"

***

2) In Section 2 (1) insert in the appropriate alphabetical order:

***

> a) **cannabis or cannabis derivative** means any part of the cannabis plant, or any cannabis preparation produced by subjecting cannabis plant material to any kind of processing.  

> b) **cannabis accessory** means any equipment, products or materials which are used, or intended for use, in the cultivation, manufacturing, testing, analyzing or packaging of cannabis, or for ingesting, inhaling or otherwise introducing cannabis into the human body.

> c) **cannabis establishment** means any business licensed to cultivate, test, manufacture, package or retail cannabis, any cannabis derivative, or cannabis accessory.

> d) **legal consumer of cannabis** means any individual aged 18 years or older who has or intends to consume legally obtained cannabis subject to the regulations detailed in Sections 7A, 9A and 14A.

***

**3. Amendment to Section 29B of the Misuse of Drugs Act 1975**

1) Repeal Section 29B.

**4. Amendment to Schedule 2 of the Misuse of Drugs Act 1975**

1) Repeal from Schedule 2, Part 1 (1) the following:

***

> "Cannabis preparations: that is, any preparation containing any tetrahydrocannabinols, including cannabis resin (commonly known as hashish) and cannabis oil (commonly know as hash oil), produced by subjecting cannabis plant material to any kind of processing."

***

**5. Amendment to Schedule 3 of the Misuse of Drugs Act 1975**

1) Repeal from Schedule 3, Part 1 (1) the following:

***

> "Cannabis fruit."

> "Cannabis plant (whether fresh, dried, or otherwise): that is, any part of any plant of the genus Cannabis except a part from which all the resin has been extracted."

> "Cannabis seed."

***

**6. Amendment to Schedule 5 of the Misuse of Drugs Act 1975**

1) Repeal from Schedule 5 (1) the following:

***

> "Any cannabis preparation (as described in Schedule 2): 5g or 100 cigarettes containing the drug."

> "Cannabis plant (as described in Schedule 3): 28g or 100 cigarettes containing the drug."

***

**7. Amendment to Section 10 of the Misuse of Drugs Amendment Act 1978**

1) Repeal from Section 10 (1) the following:

***

> "**dealing in cannabis on a substantial scale** means:
dealing (in any of the ways referred to in section 6(1) of the principal Act) with a substantial amount of a controlled drug in respect of which a prescribed cannabis offence may be committed: cultivation of a prohibited plant (being a prohibited plant in respect of which a prescribed cannabis offence may be committed) on a substantial scale"

> "**prescribed cannabis offence** means an offence against— section 6 of the principal Act in relation to a Class C controlled drug specified or described in Part 1 of Schedule 3 of the principal Act (other than catha edulis plant or coca leaf); or section 9 of the principal Act in relation to a prohibited plant of the genus Cannabis."

***

**8. Amendment to Section 4 of the Misuse of Drugs (Prohibition of Cannabis Utensils and Methamphetamine Utensils) Notice 2003**

1) Repeal Section 4.

2) New section 6A inserted (Sale of cannabis):

***

> a. A legal consumer of cannabis may not sell cannabis or cannabis derivatives to any individual or organisation.

> b. A cannabis establishment may sell cannabis, cannabis derivatives or cannabis accessories subject to the regulations of Section 14A.

> c. A cannabis establishment involved solely in the cultivation, manufacturing or packaging of cannabis - or any similar activity that does not involve retailing - may only sell to licensed cannabis retailers.

> d. Any legal consumer of cannabis found guilty of selling cannabis may be subject to a fine of no more than $14,000, dependant on the scale or severity of the crime.

> e. Any cannabis establishment involved solely in the cultivation, manufacturing or packaging of cannabis who is found guilty of selling to individuals or organisations other than licensed cannabis retailers may be subject to a fine of no more than $28,000, dependant on the scale or severity of the crime.

***

3) New section 7A inserted (Possession, transport and use of cannabis and cannabis accessories):

***

> a. A legal consumer of cannabis may possess no more than 29g of cannabis flower or dried leaf at any one point in time, unless it is harvested from privately cultivated cannabis plants at which point there is no limit provided it remains where it was grown.

> b. A legal consumer of cannabis may possess no more than 800mg of cannabis concentrate (meaning hashish, hash oil, kief or tincture) or of THC in the form of edible products.

> c. A legal consumer of cannabis may possess any number of cannabis accessories provided they are for personal consumption or legal cultivation.

> d. A legal consumer of cannabis may transport no more than 29g of cannabis flower and dried leaf or 800mg of concentrate at any one time. Any individual found guilty of breaching the above limits may be subject to a fine of no more than $500.

> e. Use of cannabis by a legal consumer is subject to the same laws that govern driving under the influence of alcohol or substances prohibited by the Misuse of Drugs Act 1975. It is also subject to the Smoke-free Environments Act 1990 and local bylaws that seek to minimise the externalities of tobacco smoking.

***

4) New section 9A inserted (Cultivation of cannabis and testing, manufacturing or packaging of cannabis products):

***

> a. Cannabis plants may only be cultivated in a secure and locked space.

> b. A legal consumer of cannabis may possess and cultivate up to 6 cannabis plants.

> c. A cannabis establishment may possess and cultivate an unlimited number of cannabis plants.

> d. A cannabis establishment may test, manufacture or package cannabis or cannabis derivatives subject to the regulations of Section 14A.

***

5) New section 14A inserted (Licensing for and regulations governing cannabis establishments):

***

> a. Licenses to commercially cultivate, or to test, manufacture, package or sell cannabis or cannabis derivatives may be issued by the Ministry of Health. Application fees may not exceed more than $500.

> b. Cannabis establishments may only sell to legal consumers of cannabis, meaning individuals over the age of 18 years. Cannabis establishments are therefore required to ask for identification from customers before sale, subject to the same regulations and penalties detailed in the Smoke-free Environments Amendment Act 1997.

> c. Cannabis establishments may not sell to an individual who is either intoxicated, or who has been known to have purchased an amount of cannabis that is the legal limit of possession - detailed in Section 7A (1) and (2) - earlier in the day. Cannabis establishments which knowingly breach this regulation may have their licenses revoked and could face a fine of no more than $2000.

***
